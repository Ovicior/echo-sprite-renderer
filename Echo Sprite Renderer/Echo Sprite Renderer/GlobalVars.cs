﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Echo_Sprite_Renderer
{
    static class GlobalVars
    {
        //Stores color that is used for the background.
        public static Microsoft.Xna.Framework.Color CurrentBackgroundColor = Color.Maroon;

        //Stores variables that modify the render/camera.
        public static float Width, Height, Scale, PosX, PosY, PosZ;

        //Stores the 3D FBX model being rendered.
        public static Model CurrentModel;

        //Stores the path of the current object.
        public static string CurrentModelPath;

        //A flag used to determine whether the model is to be changed or not.
        public static bool HasModelChanged = false;

        /// <summary>
        /// Uses flag 'HasModelChanged' and path 'CurrentModelPath' to make SpriteRenderer change CurrentModel.
        /// </summary>
        /// <param name="path"></param>
        public static void LoadNewModel(string path)
        {
            CurrentModelPath = path;
            HasModelChanged = true;
        }
    }
}
