﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Framework.WpfInterop;
using MonoGame.Framework.WpfInterop.Input;
using Xceed.Wpf.Toolkit;
using Color = System.Windows.Media.Color;
using Matrix = Microsoft.Xna.Framework.Matrix;

namespace Echo_Sprite_Renderer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //Initialization logic.
            txtboxWidth.BaseTextBox.Text = 50.ToString();
            txtboxHeight.BaseTextBox.Text = 50.ToString();
            txtboxPosX.BaseTextBox.Text = 1.ToString();
            txtboxPosY.BaseTextBox.Text = 0.ToString();
            txtboxPosZ.BaseTextBox.Text = 0.ToString();
            txtboxScale.BaseTextBox.Text = 1.ToString();
        }

        private void ColorPicker_OnSelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            ColorPicker _colorPicker = e.Source as ColorPicker;
            Color _winColor = Colors.White;

            //Convert hex color to RGB color in Windows format.
            if (_colorPicker != null)
            {
                _winColor = (Color)ColorConverter.ConvertFromString(_colorPicker.SelectedColor.ToString());
            }
            else
                Debug.WriteLine("Error, ColorPicker does not exist.");

            //Set the variable Monogame uses to render the background to the given color after it is converted to XNA format.
            GlobalVars.CurrentBackgroundColor = new Microsoft.Xna.Framework.Color(_winColor.R, _winColor.G, _winColor.B, _winColor.A);
        }

        private void ButtonNew_OnClick(object sender, RoutedEventArgs e)
        {
            //Reset variables that modify render.
            txtboxWidth.BaseTextBox.Text = 50.ToString();
            txtboxHeight.BaseTextBox.Text = 50.ToString();
            txtboxPosX.BaseTextBox.Text = 1.ToString();
            txtboxPosY.BaseTextBox.Text = 0.ToString();
            txtboxPosZ.BaseTextBox.Text = 0.ToString();
            txtboxScale.BaseTextBox.Text = 1.ToString();

            GlobalVars.CurrentModel = null;
        }

        private void ButtonOpen_OnClick(object sender, RoutedEventArgs e)
        {
            string _filePath = null;

            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog _dlg = new Microsoft.Win32.OpenFileDialog
            {

                // Set filter for file extension and default file extension (set for all 3D types allowed by default).
                FilterIndex = 1,

                //FORMAT: "Description Shown In Editor|.AllowedExtension"
                Filter = "3D Animation Files (*.fbx;*.xnb)|*.fbx;*.xnb|" +
                         "FBX Files (*.fbx)|*.fbx|Monogame Pipeline Files (*.xnb)|*.xnb"
            };


            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> _result = _dlg.ShowDialog();


            // Get the selected file name and store it. 
            if (_result.HasValue && _result.Value)
            {
                // Get document path. 
                _filePath = _dlg.FileName;
            }

            //A correct file was found and the path is valid.
            if (_filePath != null)
            {
                //Get the extension of the file.
                string extension = System.IO.Path.GetExtension(_dlg.FileName);

                //Make sure extension is in lower case letters and check if it's an .fbx.
                //Must process .fbx before usage. Do so using the Monogame Pipeline.
                if (extension.ToLower() == ".fbx")
                {
                    //Go to current domain and then go to premade directory with pipeline.
                    var fn = AppDomain.CurrentDomain.BaseDirectory + "FBX Converter\\MGCB.exe";

                    var process = new Process();
                    process.StartInfo.FileName = $"\"{fn}\""; //Constructs proper directory to access MGCB.exe.

                    //Arguments used by MGCB.
                    process.StartInfo.Arguments =
                        $"/outputDir:output /intermediateDir:obj /platform:Windows /config: /profile:Reach /compress:False " +
                        $"/importer:FbxImporter /processor:ModelProcessor /build:{_filePath}";

                    process.Start(); //Start MGCB.exe and generate Monogame files.

                    var _mgcontentFilePath = _filePath.Replace(".fbx", ".mgcontent");

                    //Stop thread until MGCB.exe is done generating files.
                    process.WaitForExit();

                    //Go to the same directory and find the .mgcontent file that was also generated.
                    //Delete the file to clean up excess.
                    File.Delete(_mgcontentFilePath);

                    //Change .fbx extension to nothing as Monogame only wants the file's directory + name to load .xnb.
                    //Use trick with flag to indirectly load new model at given path.
                    var _xnbFilePath = _filePath.Replace(".fbx", "");
                    GlobalVars.LoadNewModel(_xnbFilePath);
                }
                //Make sure extension is in lower case letters and check if it's an .xnb.
                //Load file normally.
                else if (extension.ToLower() == ".xnb")
                {
                    //Change .xnb extension to nothing as Monogame only wants the file's directory + name to load .xnb.
                    var _xnbFilePath = _filePath.Replace(".xnb", "");
                    GlobalVars.LoadNewModel(_xnbFilePath);
                }
            }
            else
                Debug.WriteLine("Error, filepath is null.");
        }
    }

    public class SpriteRenderer : WpfGame
    {
        private IGraphicsDeviceService graphicsDeviceManager;
        private WpfKeyboard keyboard;
        private WpfMouse mouse;

        private Matrix world;
        private Matrix view;
        private Matrix projection;

        protected override void Initialize()
        {
            // must be initialized. required by Content loading and rendering (will add itself to the Services)
            graphicsDeviceManager = new WpfGraphicsDeviceService(this);

            // wpf and keyboard need reference to the host control in order to receive input
            // this means every WpfGame control will have it's own keyboard & mouse manager which will only react if the mouse is in the control
            keyboard = new WpfKeyboard(this);
            mouse = new WpfMouse(this);

            // must be called after the WpfGraphicsDeviceService instance was created
            base.Initialize();

            // content loading now possible

            //TODO: Remove tree file from project by using file explorer.
            //Looks like the only thing needed to load 3D models is the .xnb file.
            //Loads model in current directory.
            GlobalVars.CurrentModel = Content.Load<Model>(AppDomain.CurrentDomain.BaseDirectory + "\\Model\\VertexColorTree");
        }

        protected override void Update(GameTime time)
        {
            // every update we can now query the keyboard & mouse for our WpfGame
            var _mouseState = mouse.GetState();
            var _keyboardState = keyboard.GetState();


            //Adjust scale of the world to make model bigger/smaller.
            world = Matrix.CreateScale(GlobalVars.Scale);

            //Update camera so that we are looking from the currently inputted parameters towards the origin.
            view = Matrix.CreateLookAt(new Vector3(GlobalVars.PosX, GlobalVars.PosY, GlobalVars.PosZ), 
                                       new Vector3(0, 0, 0), Vector3.UnitY);

            projection = Matrix.CreateOrthographic(GlobalVars.Width, GlobalVars.Height, -10f, 1000f);

            //Flag indicating change in model has been set.
            if (GlobalVars.HasModelChanged == true)
            {
                //Load new model using path stored in GlobalVars.
                GlobalVars.CurrentModel = Content.Load<Model>(GlobalVars.CurrentModelPath);

                //Turn off flag to avoid loading again. Saves processing power.
                GlobalVars.HasModelChanged = false;
            }
        }

        protected override void Draw(GameTime time)
        {
            graphicsDeviceManager.GraphicsDevice.Clear(GlobalVars.CurrentBackgroundColor);

            if (GlobalVars.CurrentModel != null)
                DrawModel(GlobalVars.CurrentModel, world, view, projection);
            else
                Debug.WriteLine("Error, CurrentModel is null");
        }

        private void DrawModel(Model model, Matrix world, Matrix view, Matrix projection)
        {
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.World = world;
                    effect.View = view;
                    effect.Projection = projection;
                }

                mesh.Draw();
            }
        }
    }
}
