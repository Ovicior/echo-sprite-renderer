﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace Echo_Sprite_Renderer
{
    /// <summary>
    /// Interaction logic for NumberTextBox.xaml
    /// </summary>
    public partial class NumberTextBox : UserControl
    {
        public NumberTextBox()
        {
            InitializeComponent();
        }

        string LocalLabel = "";
        string LocalTextBox = 0.ToString();

        public string Label
        {
            get { return LocalLabel; }
            set
            {
                LocalLabel = value;
                BaseLabel.Content = value;
            }
        }

        public string TextBox
        {
            get { return BaseTextBox.Text; }
            set
            {
                LocalTextBox = value;
                BaseTextBox.Text = value;
            }
        }

        public float TextBoxFloat
        {
            get { return float.Parse(BaseTextBox.Text); }
        }

        private void NumberTextBox_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9-\\.]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        /// <summary>
        /// Updates GlobalVars parameters that contain the input parameters in the text boxes on the bottom.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BaseTextBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            //TODO: Get rid of this awful code. I should not do this.

            //Manually updates GlobalVars depending on the name of the current textbox.
            //Makes sure empty values, and those with only a negative sign or decimal are not passed in.
            if (BaseTextBox.Text != "" && BaseTextBox.Text != "-" && BaseTextBox.Text != ".")
            {
                switch (Label)
                {
                    case "Width":
                        GlobalVars.Width = TextBoxFloat;
                        break;

                    case "Height":
                        GlobalVars.Height = TextBoxFloat;
                        break;

                    case "Scale":
                        GlobalVars.Scale = TextBoxFloat;
                        break;

                    case "PosX":
                        GlobalVars.PosX = TextBoxFloat;
                        break;

                    case "PosY":
                        GlobalVars.PosY = TextBoxFloat;
                        break;

                    case "PosZ":
                        GlobalVars.PosZ = TextBoxFloat;
                        break;

                    default:
                        Debug.WriteLine("Error, Label of NumberTextBox is incorrect or textbox is empty/invalid.");
                        break;
                }
            }
        }
    }

    public class CameraVariables : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public float Width
        {
            get { return width; }
            set { width = GlobalVars.Width; }
        }

        private float width;
    }
}
